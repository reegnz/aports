# Maintainer: Jeff Dickey <alpine@rtx.pub>
pkgname=rtx
pkgver=2023.11.8
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://rtx.pub"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdxcode/rtx/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	git init .
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin rtx
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rtx \
		-t "$pkgdir"/usr/bin/
}

sha512sums="
e148cc6180884f73b17f63c5aa7fbae8b2167afc4fed71892f4894bab3403584d7a8a9a77006ab4ed9f657967a953a6d788f9b19ef338b5e4816cb1cc4803383  rtx-2023.11.8.tar.gz
"
