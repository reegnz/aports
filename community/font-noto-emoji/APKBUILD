# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=font-noto-emoji
pkgver=2.041
pkgrel=0
pkgdesc="Google Noto emoji fonts"
url="https://github.com/googlefonts/noto-emoji"
arch="noarch"
license="OFL-1.1"
source="https://github.com/googlefonts/noto-emoji/archive/v$pkgver/font-noto-emoji-$pkgver.tar.gz"
options="!check" # No code to test
builddir="$srcdir/noto-emoji-$pkgver"

# Prior to commit 213931dec8bb08b1d4e500bf06f3892d711e9499 we build
# font-noto-emoji from source. However, some makedependencies were not
# available on all architectures and building everything from source
# blocks some builders for some time, hence using the pre-built now.

package() {
	install -Dm644 -t "$pkgdir"/usr/share/fonts/noto \
		fonts/NotoColorEmoji.ttf
}

sha512sums="
6043a80aaff8d5b20fc580721c63f249da02a157f297852c0c30bd8b335d841ab25dcffa5c0c5b3a51f49f1e98d9f110d54c7157f461a5767c710f9002302f4e  font-noto-emoji-2.041.tar.gz
"
